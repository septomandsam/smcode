#include "Date.h"
#include <iostream>
using namespace std;

Date::Date()
{

}

Date::Date(int dayNew, int monthNew, int yearNew)
{
	day = dayNew;
	month = monthNew;
	year = yearNew;

	while (day < DAY_MIN || day > DAY_MAX)
	{
		setDay(day);
	}

	while (month < MONTH_MIN || month > MONTH_MAX)
	{
		setMonth(month);
	}

	while (year < 1900 || year > 2100)
	{
		setYear(year);
	}
}

Date::~Date()
{

}

void Date::setDate(int d, int m, int y)
{
	day = d;
	month = m;
	year = y;

	while (day < DAY_MIN || day > DAY_MAX)
	{
		setDay(day);
	}

	while (month < MONTH_MIN || month > MONTH_MAX)
	{
		setMonth(month);
	}

	while (year < 1900 || year > 2100)
	{
		setYear(year);
	}
}

Date Date::getDate()
{
	return Date();
}

void Date::setDay(int d)
{
	day = d;

	while (day < DAY_MIN || day > DAY_MAX)
	{
		cout << "Day incompatible. Enter new day number.\n";
		cin >> day;
	}
}

int Date::getDay()
{
	return day;
}

void Date::setMonth(int m)
{
	month = m;

	while (month < MONTH_MIN || month > MONTH_MAX)
	{
		cout << "Month incompatible. Enter new month number.\n";
		cin >> month;
	}
}

int Date::getMonth()
{
	return month;
}

void Date::setYear(int y)
{
	year = y;

	while (year < 1900 || year > 2100)
	{
		cout << "Year incompatible. Enter new year.\n";
		cin >> year;
	}
}

int Date::getYear()
{
	return year;
}
