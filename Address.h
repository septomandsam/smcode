#pragma once
#include <string>
using namespace std;

class Address
{
private:
	string lineOne;
	string lineTwo;
	string town;
	string postcode;

public:
	Address();
	Address(string one, string two, string place, string code);
	~Address();

	void setLineOne(string one);
	string getLineOne();
	void setLineTwo(string two);
	string getLineTwo();
	void setTown(string place);
	string getTown();
	void setPostcode(string code);
	string getPostcode();
};

