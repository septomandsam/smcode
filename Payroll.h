#pragma once
#include "Staff.h"
#include <vector>

class Payroll
{
private:
	vector<Staff> staffList;

public:
	Payroll();
	~Payroll();

	void addStaff(Staff employee);
	void removeStaff(int index);
	Staff findStaffAtIndex(int index);
	//possibly find at other places?
	void printPayroll();
	int getPayrollSize();
};

