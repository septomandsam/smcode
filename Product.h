#pragma once
#include <string>
#include "Date.h"
using namespace std;

class Product
{
private:
	string name;
	string upc;
	float stockPrice;
	float retailPrice;
	Date stockDate;
	Date useByDate;
	int ageLimit;
	float vatRate;
	float discountRate;
	float RATE_MAX = 1;
	float RATE_MIN = 0;
	int quantity;

public:
	Product();
	~Product();

	void setName(string Name);
	string getName();

	void setUPC(string code);
	string getUPC();

	void setStockPrice(float price);
	float getStockPrice();
	void setRetailPrice(float price);
	float getRetailPrice();

	void setStockDate(Date date);
	void setStockDate(int d, int m, int y);
	Date getStockDate();
	void setUseByDate(Date date);
	void setUseByDate(int d, int m, int y);
	Date getUseByDate();

	void setAgeLimit(int age);
	int getAgeLimit();

	void setVATRate(float rate);
	float getVATRate();
	void setDiscountRate(float rate);
	float getDiscountRate();

	void setQuantity(int amount);
	int getQuantity();

};

