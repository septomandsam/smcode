#include "Payroll.h"
#include "Staff.h"

int main()
{
	Payroll database =  Payroll();
	Staff worker = Staff();
	worker.setName("Robert", "Grundy");
	worker.setDOB(11, 11, 1947);
	worker.setAddress("17 Privet Drive", "Quickville", "Swansea", "SA69 5EX");
	worker.setPhone("07588 244570");
	worker.setType(0);
	worker.setWage(7.50f, 0);
	
	database.addStaff(worker);
	database.printPayroll();

	return 0;
}