#include "StockDatabase.h"
#include <iostream>

StockDatabase::StockDatabase()
{
}

StockDatabase::~StockDatabase()
{
}

void StockDatabase::addProduct(Product product)
{
	products.push_back(product);
}

Product StockDatabase::findProduct(int index)
{
	if (products.size() >= index)
	{
		return products[index];
	}
	else
	{
		return Product();
	}
}

Product StockDatabase::findProduct(string name)
{
	for (vector<Product>::iterator it = products.begin(); it != products.end(); ++it)
	{
		if (it->getName() == name)
		{
			return *it;
		}
	}

	return Product(); //for when there are no matches
}

void StockDatabase::removeProduct(int index)
{
	if (products.size() >= index)
	{
		products.erase(products.begin() + index);
	}
	else
	{
		cout << "Stock database too small" << endl;
	}
}

void StockDatabase::editProductName(string newName, string name)
{
	for (vector<Product>::iterator it = products.begin(); it != products.end(); ++it)
	{
		if (it->getName() == name)
		{
			it->setName(newName);
			cout << "Name set" << endl;
		}
	}
}

void StockDatabase::editProductName(string newName, int index)
{
	if (products.size() >= index)
	{
		products[index].setName(newName);
	}
	else
	{
		cout << "Stock database too small" << endl;
	}
}

void StockDatabase::editRetailPrice(float newPrice, string name)
{
	for (vector<Product>::iterator it = products.begin(); it != products.end(); ++it)
	{
		if (it->getName() == name)
		{
			it->setRetailPrice(newPrice);
			cout << "Retail price set" << endl;
		}
	}
}

void StockDatabase::editRetailPrice(float newPrice, int index)
{
	if (products.size() >= index)
	{
		products[index].setRetailPrice(newPrice);
	}
	else
	{
		cout << "Stock database too small" << endl;
	}
}

void StockDatabase::editQuantity(int newQuantity, string name)
{
	for (vector<Product>::iterator it = products.begin(); it != products.end(); ++it)
	{
		if (it->getName() == name)
		{
			it->setQuantity(newQuantity);
			cout << "New quantity set" << endl;
		}
	}
}

void StockDatabase::editQuantity(int newQuantity, int index)
{
	if (products.size() >= index)
	{
		products[index].setQuantity(newQuantity);
	}
	else
	{
		cout << "Stock database too small" << endl;
	}
}

void StockDatabase::printStock()
{
	for (unsigned int i = 0; i < products.size(); i++)
	{
		cout << "Name: " << products[i].getName() << endl;
		cout << "UPC: " << products[i].getUPC() << endl;
		cout << "Stock price: " << products[i].getStockPrice() << endl;
		cout << "Retail price: " << products[i].getRetailPrice() << endl;
		cout << "Stock date: " << products[i].getStockDate().getDay() << "/" << products[i].getStockDate().getMonth() << "/" << products[i].getStockDate().getYear() << endl;
		cout << "Use-by date: " << products[i].getUseByDate().getDay() << "/" << products[i].getUseByDate().getMonth() << "/" << products[i].getUseByDate().getYear() << endl;
		cout << "Age limit: " << products[i].getAgeLimit() << endl;
		cout << "VAT rate: " << products[i].getVATRate() << endl;
		cout << "Discount rate: " << products[i].getDiscountRate() << endl;
		cout << "Quantity: " << products[i].getQuantity() << endl;

		cout << "********************\n";
	}
}

int StockDatabase::getDatabaseSize()
{
	return products.size();
}
