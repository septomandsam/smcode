#include "Wage.h"
#include <iostream>
using namespace std;

Wage::Wage()
{

}

Wage::Wage(float pounds, int every)
{
	value = pounds;
	type = static_cast<period>(every);

	while (every < 0 || every > 2)
	{
		setPeriod(every);
	}
}

Wage::~Wage()
{

}

void Wage::setWage(float pounds, int every)
{
	value = pounds;
	type = static_cast<period>(every);
}

Wage Wage::getWage()
{
	return Wage();
}

void Wage::setValue(float val)
{
	value = val;
}

float Wage::getValue()
{
	return value;
}

void Wage::setPeriod(int paytype)
{
	type = static_cast<period>(paytype);

	int temp;
	while (paytype < 0 || paytype > 2)
	{
		cout << "Pay period not supported\n";
		cout << "0 - hourly\n";
		cout << "1 - weekly\n";
		cout << "2 - yearly\n";

		cin >> temp;
		type = static_cast<period>(temp);
	}
}

int Wage::getPeriod()
{
	return type;
}
