#pragma once
#include <string>
#include "Date.h"
#include "Address.h"
#include "Wage.h"
using namespace std;

class Staff
{
private:
	string surname;
	string forename;
	Date dateOfBirth;
	Address address;
	string phoneNumber;
	enum EmployeeType { Manager, Casual };
	EmployeeType type;
	Wage wage;

public:
	Staff();
	~Staff();

	void setName(string first, string last);
	void setForename(string first);
	string getForename();
	void setSurname(string last);
	string getSurname();

	void setDOB(int d, int m, int y);
	void setDOB(Date date);
	Date getDOB();

	void setAddress(string one, string two, string three, string four);
	void setAddress(Address place);
	Address getAddress();

	void setPhone(string number);
	string getPhone();

	void setType(int stafftype);
	int getType();

	void setWage(Wage pay);
	void setWage(float value, int paytype);
	Wage getWage();
};

