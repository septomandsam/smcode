#pragma once
class Wage
{
private:
	float value;
	enum period { hour, week, year };
	period type;

public:
	Wage();
	Wage(float pounds, int every);
	~Wage();

	void setWage(float pounds, int every);
	Wage getWage();

	void setValue(float val);
	float getValue();
	void setPeriod(int paytype);
	int getPeriod();
};

