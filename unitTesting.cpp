// unitTesting.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "gtest/gtest.h"
#include "Payroll.h"
#include "Product.h"
#include "Staff.h"
#include "StockDatabase.h"

TEST(staffTest, getName) 
{
	Staff member;
	member.setName("Joe", "Bloggs");
	EXPECT_EQ(member.getForename(), "Joe");
	EXPECT_EQ(member.getSurname(), "Bloggs");
}

TEST(staffTest, getForname)
{
	Staff member;
	member.setForename("Joe");
	EXPECT_EQ(member.getForename(), "Joe");
}

TEST(staffTest, getSurname)
{
	Staff member;
	member.setSurname("Bloggs");
	EXPECT_EQ(member.getSurname(), "Bloggs");
}

TEST(staffTest, getDOB)
{
	Staff member;
	member.setDOB(11, 05, 1997);
	EXPECT_EQ(member.getDOB().getDay(), 11);
	EXPECT_EQ(member.getDOB().getMonth(), 05);
	EXPECT_EQ(member.getDOB().getYear(), 1997);
}

TEST(staffTest, getDOB_Date)
{
	Staff member;
	Date day;
	day.setDate(11, 05, 1997);
	member.setDOB(day);
	EXPECT_EQ(member.getDOB().getDay(), 11);
	EXPECT_EQ(member.getDOB().getMonth(), 05);
	EXPECT_EQ(member.getDOB().getYear(), 1997);
}

TEST(staffTest, address)
{
	Staff member;
	member.setAddress("42", "Wallaby Way", "Sydney", "CF69 5EX");
	EXPECT_EQ(member.getAddress().getLineOne(), "42");
	EXPECT_EQ(member.getAddress().getLineTwo(), "Wallaby Way");
	EXPECT_EQ(member.getAddress().getTown(), "Sydney");
	EXPECT_EQ(member.getAddress().getPostcode(), "CF69 5EX");
}

TEST(staffTest, address_Class)
{
	Staff member;
	Address here;
	here.setLineOne("42");
	here.setLineTwo("Wallaby Way");
	here.setTown("Sydney");
	here.setPostcode("CF69 5EX");
	member.setAddress(here);
	EXPECT_EQ(member.getAddress().getLineOne(), "42");
	EXPECT_EQ(member.getAddress().getLineTwo(), "Wallaby Way");
	EXPECT_EQ(member.getAddress().getTown(), "Sydney");
	EXPECT_EQ(member.getAddress().getPostcode(), "CF69 5EX");
}

TEST(staffTest, phone)
{
	Staff member;
	member.setPhone("07588 222333");
	EXPECT_EQ(member.getPhone(), "07588 222333");
}

TEST(staffTest, type)
{
	Staff member;
	member.setType(0);
	EXPECT_EQ(member.getType(), 0);
}

TEST(staffTest, wage)
{
	Staff member;
	member.setWage(8.0f, 0);
	EXPECT_EQ(member.getWage().getPeriod(), 0);
	EXPECT_EQ(member.getWage().getValue(), 8.0f);
}

TEST(staffTest, wage_object)
{
	Staff member;
	Wage pay;
	pay.setWage(8.0f, 0);
	member.setWage(pay);
	EXPECT_EQ(member.getWage().getPeriod(), 0);
	EXPECT_EQ(member.getWage().getValue(), 8.0f);
}

TEST(payroll, add_staff)
{
	Payroll list;
	Staff member;
	member.setName("Robert", "Grundy");
	member.setDOB(03, 11, 1997);
	member.setPhone("07588 374757");
	list.addStaff(member);
	EXPECT_EQ(list.findStaffAtIndex(0).getForename(), "Robert");
	EXPECT_EQ(list.findStaffAtIndex(0).getSurname(), "Grundy");
}

TEST(payroll, remove_staff)
{
	Payroll list;
	Staff one;
	Staff two;
	Staff three;
	list.addStaff(one);
	list.addStaff(two);
	list.addStaff(three);
	list.removeStaff(1);
	EXPECT_EQ(list.getPayrollSize(), 2);
}

TEST(product, name)
{
	Product thing;
	thing.setName("Cheese Tits");
	EXPECT_EQ(thing.getName(), "Cheese Tits");
}

TEST(product, code)
{
	Product thing;
	thing.setUPC("1234567890");
	EXPECT_EQ(thing.getUPC(), "1234567890");
}

TEST(product, stockPrice)
{
	Product thing;
	thing.setStockPrice(7.40f);
	EXPECT_EQ(thing.getStockPrice(), 7.40f);
}

TEST(product, retailPrice)
{
	Product thing;
	thing.setRetailPrice(9.40f);
	EXPECT_EQ(thing.getRetailPrice(), 9.40f);
}

TEST(product, stockDate)
{
	Product thing;
	thing.setStockDate(29, 4, 2017);
	EXPECT_EQ(thing.getStockDate().getDay(), 29);
	EXPECT_EQ(thing.getStockDate().getMonth(), 4);
	EXPECT_EQ(thing.getStockDate().getYear(), 2017);
}

TEST(product, stockDate_object)
{
	Product thing;
	Date stock(29, 4, 2017);
	thing.setStockDate(stock);
	EXPECT_EQ(thing.getStockDate().getDay(), 29);
	EXPECT_EQ(thing.getStockDate().getMonth(), 4);
	EXPECT_EQ(thing.getStockDate().getYear(), 2017);
}

TEST(product, useByDate)
{
	Product thing;
	thing.setUseByDate(11, 5, 2017);
	EXPECT_EQ(thing.getUseByDate().getDay(), 11);
	EXPECT_EQ(thing.getUseByDate().getMonth(), 5);
	EXPECT_EQ(thing.getUseByDate().getYear(), 2017);
}

TEST(product, useByDate_object)
{
	Product thing;
	Date end(11, 5, 2017);
	thing.setUseByDate(end);
	EXPECT_EQ(thing.getUseByDate().getDay(), 11);
	EXPECT_EQ(thing.getUseByDate().getMonth(), 5);
	EXPECT_EQ(thing.getUseByDate().getYear(), 2017);
}

TEST(product, ageLimit)
{
	Product thing;
	thing.setAgeLimit(18);
	EXPECT_EQ(thing.getAgeLimit(), 18);
}

TEST(product, VATRate)
{
	Product thing;
	thing.setVATRate(0.2f);
	EXPECT_EQ(thing.getVATRate(), 0.2f);
}

TEST(product, discountRate)
{
	Product thing;
	thing.setDiscountRate(0.3f);
	EXPECT_EQ(thing.getDiscountRate(), 0.3f);
}

TEST(product, quantity)
{
	Product thing;
	thing.setQuantity(2);
	EXPECT_EQ(thing.getQuantity(), 2);
}

TEST(database, addProduct)
{
	StockDatabase database;
	Product thing;
	database.addProduct(thing);
	EXPECT_EQ(database.getDatabaseSize(), 1);
}

TEST(database, findProduct_index)
{
	StockDatabase database;
	Product thingOne;
	Product thingTwo;
	Product thingThree;

	thingOne.setName("Cheese Tits");
	thingTwo.setName("Corn Boners");
	thingThree.setName("Trump Tarts");

	database.addProduct(thingOne);
	database.addProduct(thingTwo);
	database.addProduct(thingThree);

	EXPECT_EQ(database.findProduct(1).getName(), "Corn Boners");
}

TEST(database, findProduct_name)
{
	StockDatabase database;
	Product thingOne;
	Product thingTwo;
	Product thingThree;

	thingOne.setName("Cheese Tits");
	thingTwo.setName("Corn Boners");
	thingThree.setName("Trump Tarts");

	database.addProduct(thingOne);
	database.addProduct(thingTwo);
	database.addProduct(thingThree);

	EXPECT_EQ(database.findProduct("Cheese Tits").getName(), "Cheese Tits");
}

TEST(database, removeProduct)
{
	StockDatabase database;
	Product thingOne;
	Product thingTwo;
	Product thingThree;

	thingOne.setName("Cheese Tits");
	thingTwo.setName("Corn Boners");
	thingThree.setName("Trump Tarts");

	database.addProduct(thingOne);
	database.addProduct(thingTwo);
	database.addProduct(thingThree);

	database.removeProduct(1);
	EXPECT_EQ(database.getDatabaseSize(), 2);
}

TEST(database, editName_index)
{
	StockDatabase database;
	Product thingOne;
	Product thingTwo;
	Product thingThree;

	thingOne.setName("Cheese Tits");
	thingTwo.setName("Corn Boners");
	thingThree.setName("Trump Tarts");

	database.addProduct(thingOne);
	database.addProduct(thingTwo);
	database.addProduct(thingThree);

	database.editProductName("Corn Horners", 1);
	EXPECT_EQ(database.findProduct(1).getName(), "Corn Horners");
}

TEST(database, editName_string)
{
	StockDatabase database;
	Product thingOne;
	Product thingTwo;
	Product thingThree;

	thingOne.setName("Cheese Tits");
	thingTwo.setName("Corn Boners");
	thingThree.setName("Trump Tarts");

	database.addProduct(thingOne);
	database.addProduct(thingTwo);
	database.addProduct(thingThree);

	database.editProductName("Tramp Tarts", "Trump Tarts");
	EXPECT_EQ(database.findProduct(2).getName(), "Tramp Tarts");
}

TEST(database, editPrice_index)
{
	StockDatabase database;
	Product thingOne;
	Product thingTwo;
	Product thingThree;

	thingOne.setName("Cheese Tits");
	thingOne.setRetailPrice(0.7f);
	thingTwo.setName("Corn Boners");
	thingTwo.setRetailPrice(0.8f);
	thingThree.setName("Trump Tarts");
	thingThree.setRetailPrice(1.2f);

	database.addProduct(thingOne);
	database.addProduct(thingTwo);
	database.addProduct(thingThree);

	database.editRetailPrice(1.4f, 2);
	EXPECT_EQ(database.findProduct("Trump Tarts").getRetailPrice(), 1.4f);
}

TEST(database, editPrice_string)
{
	StockDatabase database;
	Product thingOne;
	Product thingTwo;
	Product thingThree;

	thingOne.setName("Cheese Tits");
	thingOne.setRetailPrice(0.7f);
	thingTwo.setName("Corn Boners");
	thingTwo.setRetailPrice(0.8f);
	thingThree.setName("Trump Tarts");
	thingThree.setRetailPrice(1.2f);

	database.addProduct(thingOne);
	database.addProduct(thingTwo);
	database.addProduct(thingThree);

	database.editRetailPrice(0.9f, "Corn Boners");
	EXPECT_EQ(database.findProduct(1).getRetailPrice(), 0.9f);
}

TEST(database, editQuantity_index)
{
	StockDatabase database;
	Product thingOne;
	Product thingTwo;
	Product thingThree;

	thingOne.setName("Cheese Tits");
	thingOne.setQuantity(5);
	thingTwo.setName("Corn Boners");
	thingTwo.setQuantity(20);
	thingThree.setName("Trump Tarts");
	thingThree.setQuantity(2);

	database.addProduct(thingOne);
	database.addProduct(thingTwo);
	database.addProduct(thingThree);

	database.editQuantity(24, 2);
	EXPECT_EQ(database.findProduct("Trump Tarts").getQuantity(), 24);
}

TEST(database, editQuantity_string)
{
	StockDatabase database;
	Product thingOne;
	Product thingTwo;
	Product thingThree;

	thingOne.setName("Cheese Tits");
	thingOne.setQuantity(5);
	thingTwo.setName("Corn Boners");
	thingTwo.setQuantity(20);
	thingThree.setName("Trump Tarts");
	thingThree.setQuantity(2);

	database.addProduct(thingOne);
	database.addProduct(thingTwo);
	database.addProduct(thingThree);

	database.editQuantity(10, "Cheese Tits");
	EXPECT_EQ(database.findProduct(0).getQuantity(), 10);
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

