#include "Staff.h"

Staff::Staff()
{

}

Staff::~Staff()
{

}

void Staff::setName(string first, string last)
{
	forename = first;
	surname = last;
}

void Staff::setForename(string first)
{
	forename = first;
}

string Staff::getForename()
{
	return forename;
}

void Staff::setSurname(string last)
{
	surname = last;
}

string Staff::getSurname()
{
	return surname;
}

void Staff::setDOB(int d, int m, int y)
{
	dateOfBirth.setDate(d, m, y);
}

void Staff::setDOB(Date date)
{
	dateOfBirth.setDay(date.getDay());
	dateOfBirth.setMonth(date.getMonth());
	dateOfBirth.setYear(date.getYear());
}

Date Staff::getDOB()
{
	return dateOfBirth;
}

void Staff::setAddress(string one, string two, string three, string four)
{
	address.setLineOne(one);
	address.setLineTwo(two);
	address.setTown(three);
	address.setPostcode(four);
}

void Staff::setAddress(Address place)
{
	address.setLineOne(place.getLineOne());
	address.setLineTwo(place.getLineTwo());
	address.setTown(place.getTown());
	address.setPostcode(place.getPostcode());
}

Address Staff::getAddress()
{
	return address;
}

void Staff::setPhone(string number)
{
	phoneNumber = number;
}

string Staff::getPhone()
{
	return phoneNumber;
}

void Staff::setType(int stafftype)
{
	type = static_cast<EmployeeType>(stafftype);
}

int Staff::getType()
{
	return type;
}

void Staff::setWage(Wage pay)
{
	wage.setValue(pay.getValue());
	wage.setPeriod(pay.getPeriod());
}

void Staff::setWage(float value, int paytype)
{
	wage.setValue(value);
	wage.setPeriod(paytype);
}

Wage Staff::getWage()
{
	return wage;
}
