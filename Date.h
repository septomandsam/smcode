#pragma once
class Date
{
private:
	int DAY_MIN = 1;
	int DAY_MAX = 31;
	int MONTH_MIN = 1;
	int MONTH_MAX = 12;

	int day;
	int month;
	int year;

public:
	Date();
	Date(int dayNew, int monthNew, int yearNew);
	~Date();

	void setDate(int d, int m, int y);
	Date getDate();

	void setDay(int d);
	int getDay();
	void setMonth(int m);
	int getMonth();
	void setYear(int y);
	int getYear();
};

