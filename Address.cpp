#include "Address.h"

Address::Address()
{

}

Address::Address(string one, string two, string place, string code)
{
	lineOne = one;
	lineTwo = two;
	town = place;
	postcode = code;
}

Address::~Address()
{

}

void Address::setLineOne(string one)
{
	lineOne = one;
}

string Address::getLineOne()
{
	return lineOne;
}

void Address::setLineTwo(string two)
{
	lineTwo = two;
}

string Address::getLineTwo()
{
	return lineTwo;
}

void Address::setTown(string place)
{
	town = place;
}

string Address::getTown()
{
	return town;
}

void Address::setPostcode(string code)
{
	postcode = code;
}

string Address::getPostcode()
{
	return postcode;
}
