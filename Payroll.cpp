#include "Payroll.h"
#include <iostream>
using namespace std;

Payroll::Payroll()
{

}

Payroll::~Payroll()
{

}

void Payroll::addStaff(Staff employee)
{
	staffList.push_back(employee);
}

void Payroll::removeStaff(int index)
{
	if (staffList.size() >= index)
	{
		staffList.erase(staffList.begin() + index);
	}
	else
	{
		cout << "Payroll too small" << endl;
	}
}

Staff Payroll::findStaffAtIndex(int index)
{
	if (staffList.size() >= index)
	{
		return staffList[index];
	}
	else
	{
		return Staff();
	}
}

void Payroll::printPayroll()
{
	for (unsigned int i = 0; i < staffList.size(); i++)
	{
		cout << "Employee number " << i + 1 << "\n";
		cout << "Name: " << staffList[i].getForename() << " " << staffList[i].getSurname() << "\n";
		cout << "Date of birth: " << staffList[i].getDOB().getDay() << "/" << staffList[i].getDOB().getMonth() << "/" 
			<< staffList[i].getDOB().getYear() << "\n";
		cout << "Address: " << staffList[i].getAddress().getLineOne() << ", " << staffList[i].getAddress().getLineTwo() << ", " 
			<< staffList[i].getAddress().getTown() << ", " << staffList[i].getAddress().getPostcode() << "\n";
		cout << "Phone number: " << staffList[i].getPhone() << "\n";
		cout << "Staff type: ";
		if (staffList[i].getType() == 0)
		{
			cout << "Manager\n";
		}
		else
		{
			cout << "Casual\n";
		}
		cout << "Wage: " << staffList[i].getWage().getValue();
		switch (staffList[i].getWage().getPeriod())
		{
		case 0:
			cout << " hourly\n";
			break;
		case 1:
			cout << " weekly\n";
			break;
		case 2:
			cout << " yearly\n";
			break;
		default:
			cout << "*payrate not available*\n";
			break;
		}

		cout << "********************\n";
	}
}

int Payroll::getPayrollSize()
{
	return staffList.size();
}
