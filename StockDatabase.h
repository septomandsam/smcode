#pragma once
#include "Product.h"
#include <vector>

class StockDatabase
{
private:
	vector<Product> products;

public:
	StockDatabase();
	~StockDatabase();

	void addProduct(Product product);
	Product findProduct(int index);
	Product findProduct(string name);
	void removeProduct(int index);

	void editProductName(string newName, string name);
	void editProductName(string newName, int index);
	void editRetailPrice(float newPrice, string name);
	void editRetailPrice(float newPrice, int index);
	void editQuantity(int newQuantity, string name);
	void editQuantity(int newQuantity, int index);

	void printStock();
	int getDatabaseSize();

};

