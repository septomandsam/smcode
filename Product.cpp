#include "Product.h"
#include <iostream>
using namespace std;

Product::Product()
{
}

Product::~Product()
{
}

void Product::setName(string Name)
{
	name = Name;
}

string Product::getName()
{
	return name;
}

void Product::setUPC(string code)
{
	upc = code;
}

string Product::getUPC()
{
	return upc;
}

void Product::setStockPrice(float price)
{
	stockPrice = price;
}

float Product::getStockPrice()
{
	return stockPrice;
}

void Product::setRetailPrice(float price)
{
	retailPrice = price;
}

float Product::getRetailPrice()
{
	return retailPrice;
}

void Product::setStockDate(Date date)
{
	stockDate = date;
}

void Product::setStockDate(int d, int m, int y)
{
	stockDate.setDay(d);
	stockDate.setMonth(m);
	stockDate.setYear(y);
}

Date Product::getStockDate()
{
	return stockDate;
}

void Product::setUseByDate(Date date)
{
	useByDate = date;
}

void Product::setUseByDate(int d, int m, int y)
{
	useByDate.setDay(d);
	useByDate.setMonth(m);
	useByDate.setYear(y);
}

Date Product::getUseByDate()
{
	return useByDate;
}

void Product::setAgeLimit(int age)
{
	ageLimit = age;
}

int Product::getAgeLimit()
{
	return ageLimit;
}

void Product::setVATRate(float rate)
{
	vatRate = rate;

	if (vatRate < 0.0f || vatRate > 1.0f)
	{
		cout << "VAT rate outside range" << endl;
		cout << "Must be floating point number between 0 and 1" << endl;
	}
}

float Product::getVATRate()
{
	return vatRate;
}

void Product::setDiscountRate(float rate)
{
	discountRate = rate;

	if (discountRate < 0.0f || discountRate > 1.0f)
	{
		cout << "Discount rate outside range" << endl;
		cout << "Must be floating point number between 0 and 1" << endl;
	}
}

float Product::getDiscountRate()
{
	return discountRate;
}

void Product::setQuantity(int amount)
{
	quantity = amount;
}

int Product::getQuantity()
{
	return quantity;
}

